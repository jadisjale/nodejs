const db = require('./db');

const Usuario = db.sequelize.define(
    'usuarios',
    {
        login: {
            type: db.Sequelize.STRING
        },
        senha: {
            type: db.Sequelize.STRING
        }
    }
);

Usuario.sync({force: true});

module.exports = Usuario;
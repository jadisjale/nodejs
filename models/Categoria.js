const db = require('./db');

const Categoria = db.sequelize.define(
    'categorias',
    {
        nome: {
            type: db.Sequelize.STRING
        },
        slug: {
            type: db.Sequelize.STRING
        }
    }
);

// Categoria.sync({force: true});

module.exports = Categoria;
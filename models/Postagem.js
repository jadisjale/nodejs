const db = require('./db');

const Postagem = db.sequelize.define(
    'postagens', {
        titulo: {
            type: db.Sequelize.STRING
        },
        conteudo: {
            type: db.Sequelize.TEXT
        },
        categoriaId: {
            type: db.Sequelize.INTEGER
        }
    }
);

module.exports = Postagem;

// Postagem.sync({force: true});
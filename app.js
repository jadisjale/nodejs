const express = require('express');
const app = express();
const handlebars = require('express-handlebars');
const bodyParser = require('body-parser');
const Postagem = require('./models/Postagem');
const Categoria = require('./models/Categoria');
const path = require('path');
const admin = require('./routes/admin');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
require('./config/auth')(passport);
const bcrypt = require('bcryptjs');
const Usuario = require('./models/Usuario');
const {isAdmin} = require('./helpers/eAdmin');



//sessão
app.use(session({
    secret: "112233", //deve ser segura
    resave: false,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use((req, res, next) => {
    res.locals.success_msg = req.flash("success_msg");
    res.locals.error_msg = req.flash("error_msg");
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

//configuração template
app.engine('handlebars', handlebars(
    {defaultLayout: 'main'}
));
app.set('view engine', 'handlebars');

//configuracao post
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//public
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Rotas
 */
app.get('/', (req, res) => {
    res.render('public/index');
});

app.get('/postagens', (req, res) => {
    Categoria.hasMany(Postagem);
    Postagem.belongsTo(Categoria);
    Postagem.findAll({ include: Categoria }).then((postagens) => {
        res.render('public/postagens', {'postagens': postagens});
    });
});

app.get('/login', (req, res) => {
    res.render('public/login');
});

app.post('/login', (req, res, next) => {

    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    })(req, res, next);

});

app.get('/novo-usuario', isAdmin, (req, res) => {
    res.render('public/usuario');
});

app.post('/novo-usuario', (req, res) => {

    var erros = [];

    if (!req.body.login || typeof req.body.login == undefined || req.body.login == null) {
        erros.push({
            text: 'Login não informado'
        });
    }

    if (!req.body.senha || typeof req.body.senha == undefined || req.body.senha == null) {
        erros.push({
            text: 'Senha não informado'
        });
    }

    if (erros.length > 0) {
        res.render('public/usuario', {'erros': erros});
    } else {

        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(req.body.senha, salt, (err, hash) => {
                if (err) {
                    req.flash("error_msg", "Erro ao criptografar senha do usuario");
                    res.redirect('/novo-usuario');
                }
                req.body.senha = hash;

                Usuario.create(req.body).then(() => {
                    req.flash("success_msg", "Usuário adicionada com sucesso");
                    res.redirect('/novo-usuario');
                }).catch((erro) => {
                    req.flash("error_msg", erro);
                    res.redirect('/novo-usuario');
                });

            });
        });
    }

});

app.get('/logout', (req, res) => {
   req.logout();
   req.flash('success_msg', 'Deslogado');
   res.redirect('/');
});

app.use('/admin', admin);

//padrão
const PORT = process.env.PORT || 8081;
app.listen(PORT, function(){
    console.log('servidor on');
});
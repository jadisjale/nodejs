const Postagem = sequelize.define(
    'postagens', {
        titulo: {
            type: Sequelize.STRING
        },
        conteudo: {
            type: Sequelize.TEXT
        }
    }
);

const Usuario = sequelize.define(
    'usuarios', {
        nome: {
            type: Sequelize.STRING
        },
        sobre_nome: {
            type: Sequelize.STRING
        },
        idade: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        }
    }
);

Postagem.create({
    titulo: 'Título da postagem',
    conteudo: 'Conteúdo da postagem'
});

Usuario.create({
    nome: 'Jadis',
    sobre_nome: 'Jale',
    idade: 28,
    email: 'jadisjale@gmail.com'
});
const express = require('express');
const rotas = express.Router();
const Categoria = require('../models/Categoria');
const Postagem = require('../models/Postagem');

rotas.get('/categorias', (req, res) => {
    Categoria.findAll().then((categorias) => {
        res.render('admin/categoria', {'categorias': categorias});
    });
});

rotas.get('/categoria', (req, res) => {
    res.render('admin/nova_categoria');
});

rotas.post('/categoria', (req, res) => {

    var erros = [];

    if (!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null) {
        erros.push({
            text: 'Nome inválido'
        });
    }

    if (!req.body.slug || typeof req.body.slug == undefined || req.body.slug == null) {
        erros.push({
            text: 'Slug inválido'
        });
    }

    if (erros.length > 0) {
        res.render('admin/nova_categoria', {'erros': erros});
    } else if (req.params.id) {
        Categoria.update({
            nome: req.body.nome,
            slug: req.body.slug
        }, {
            where : {
                id: req.params.id
            }
        })
        .then(() => {
            req.flash("success_msg", "Categoria alterada com sucesso");
            res.redirect('/admin/categoria/' + req.params.id);
        })
        .catch((error) => {
            req.flash("error_msg", "Categoria não cadastrada");
            res.redirect('/admin/categoria/' + req.params.id);
        });
    } else {
        Categoria.create(req.body)
        .then(() => {
            req.flash("success_msg", "Categoria cadastrada");
            res.redirect('/admin/categoria');
        })
        .catch((error) => {
            req.flash("error_msg", "Categoria não cadastrada");
            res.redirect('/admin/categoria');
        });
    }
});

rotas.get('/categoria/:id', (req, res) => {
    Categoria.findOne({
        where: {
            id: req.params.id
        }
    })
    .then( (categoria) => {
        // res.send(categoria);
        res.render("admin/nova_categoria", {"categoria": categoria});
    })
    .catch( (error) => {
        req.flash("error_msg", "Categoria não encontrada");
        res.render("admin/nova_categoria", {"categoria": categoria});
    });
});

rotas.post('/categoria/:id', (req, res) => {

    var erros = [];

    if (!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null) {
        erros.push({
            text: 'Nome inválido'
        });
    }

    if (!req.body.slug || typeof req.body.slug == undefined || req.body.slug == null) {
        erros.push({
            text: 'Slug inválido'
        });
    }

    if (erros.length > 0) {
        console.log('tem erros');
        res.render('admin/nova_categoria', {'erros': erros});
    } else if (req.params.id) {
        Categoria.update({
            nome: req.body.nome,
            slug: req.body.slug
        }, {
            where : {
                id: req.params.id
            }
        })
        .then(() => {
            req.flash("success_msg", "Categoria alterada com sucesso");
            res.redirect('/admin/categoria/' + req.params.id);
        })
        .catch((error) => {
            req.flash("error_msg", "Categoria não cadastrada");
            res.redirect('/admin/categoria/' + req.params.id);
        });
    } else {
        Categoria.create(req.body)
        .then(() => {
            req.flash("success_msg", "Categoria cadastrada");
            res.redirect('/admin/nova_categoria');
        })
        .catch((error) => {
            req.flash("error_msg", "Categoria não cadastrada");
            res.redirect('/admin/nova_categoria');
        });
    }
});

rotas.post('/categoria/deletar/:id', (req, res) => {
    var erros = [];

    if (!req.params.id || typeof req.params.id == undefined || req.params.id == null) {
        erros.push({
            text: 'Id inválido'
        });
    }

    console.log(erros);

    if (erros.length > 0) {
        res.render('admin/categoria', {'erros': erros});
    } else {
        Categoria.destroy({
            where : {
                id : req.params.id
            }
        })
        .then(() => {
            req.flash("success_msg", "Categoria deletada com sucesso");
            res.redirect('/admin/categorias');
        }).catch((error) => {
            req.flash("success_error", "Erro ao deletar categoria");
            res.redirect('/admin/categorias');
        });
    }

});

rotas.get('/postagens', (req, res) => {
    Categoria.hasMany(Postagem);
    Postagem.belongsTo(Categoria);
    Postagem.findAll({ include: Categoria }).then((postagens) => {
        console.log(postagens);
        res.render('admin/lista_postagens', {'postagens': postagens});
    });
});

rotas.get('/postagem', (req, res) => {
    Categoria.findAll().then((categorias) => {
        res.render('admin/adicionar_postagem', {'categorias': categorias});
    });
});

rotas.post('/postagem', (req, res) => {
    var erros = [];

    if (!req.body.titulo || typeof req.body.titulo == undefined || req.body.titulo == null) {
        erros.push({
            text: 'Título inválido'
        });
    }

    if (!req.body.conteudo || typeof req.body.conteudo == undefined || req.body.conteudo == null) {
        erros.push({
            text: 'Conteúdo inválido'
        });
    }

    if (!req.body.categoriaId || typeof req.body.categoriaId == undefined || req.body.categoriaId == null) {
        erros.push({
            text: 'Categoria inválido'
        });
    }

    if (erros.length > 0) {
        console.log('tem erros');
        res.render('admin/adicionar_postagem', {'erros': erros});
    } else {
        Postagem.create(req.body)
        .then(() => {
            req.flash("success_msg", "Postagem adicionada com sucesso");
            res.redirect('/admin/postagem');
        }).catch((error) => {
            req.flash("error_msg", "Erro ao cadastrar nova postagem");
            res.redirect('/admin/postagem');
        });
    }
});

module.exports = rotas;
const localStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const Usuario = require('../models/Usuario');

module.exports = function (passport) {

    passport.use(new localStrategy({usernameField: 'login', passwordField: 'senha'}, function (email, senha, done) {
        console.log(email, senha);
        Usuario.findOne({
            where: {
                login: email
            }
        }).then( (usuario) => {
            if (!usuario) {
                return done(null, false, {message: 'Essa conta não existe'});
            }

            bcrypt.compare(senha, usuario.senha, (erro, batem) => {
                if (batem) {
                    return done(null, usuario);
                }

                return done(null, false, {message: 'Senha incorreta'})
            });

        }).catch( (error) => {
            return done(null, false, {message: 'Ocorreu um erro ao tentar logar'})
        });
    }));

    passport.serializeUser((usuario, done) => {
        done(null, usuario.id)
    });

    passport.deserializeUser(function(id, done) {
        Usuario.findByPk(id).then(function(usuario) {
            if (usuario) {
                done(null, usuario.get());
            } else {
                done(usuario.errors, null);
            }
        });
    });

}
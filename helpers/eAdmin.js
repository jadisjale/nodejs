module.exports = {
    isAdmin: function (req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        } else {
            req.flash('error_msg', 'Você deve estar logado para entrar aqui');
            res.redirect('/');
        }
    }
}